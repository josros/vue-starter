import { CloudLogger } from "@/services/logging/logger-cloud";
import { StackdriverLogger } from "@josros/stackdriver-logger";
import uuid from "uuid/v4";

const mockUuid = "9509915d-e9d3-4d15-bdee-4cdeae24mock";

jest.mock("@josros/stackdriver-logger");
jest.mock("uuid/v4");

function wait(ms: number): Promise<void> {
  return new Promise((resolve): void => {
    setTimeout(resolve, ms);
  });
}

describe("CloudLogger", (): void => {
  beforeEach((): void => {
    jest.resetAllMocks();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (uuid as any).mockImplementation(() => mockUuid);
  });

  it("write in correct format", async (): Promise<void> => {
    // GIVEN
    const testlog = "foo bar";
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue("");

    // WHEN
    const logger = new CloudLogger();
    logger.log(testlog, [{ key: "testKey", value: "testValue" }]);
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).toHaveBeenCalledWith([
      {
        severity: 200,
        labels: {
          testKey: "testValue",
          deviceKey: mockUuid
        },
        jsonPayload: {
          text: testlog
        }
      }
    ]);
  });

  it("write critical log", async (): Promise<void> => {
    // GIVEN
    const testlog = "foo bar";
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue("");

    // WHEN
    const logger = new CloudLogger();
    logger.critical(testlog);
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).toHaveBeenCalledWith([
      {
        severity: 600,
        labels: {
          deviceKey: mockUuid
        },
        jsonPayload: {
          text: testlog
        }
      }
    ]);
  });

  it("do not flush if no log are there", async (): Promise<void> => {
    // GIVEN
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue("");

    // WHEN
    const logger = new CloudLogger();
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).not.toBeCalled();
  });

  it("flush logs after interval has past", async (): Promise<void> => {
    // GIVEN
    const INTERVAL = 500;

    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue("");
    const testlog1 = "foo bar";
    const testlog2 = "bar foo";

    // WHEN
    const logger = new CloudLogger(INTERVAL, true);
    logger.log(testlog1);
    logger.log(testlog2);

    await wait(2.1 * INTERVAL);

    // THEN
    expect(StackdriverLogger.prototype.log).toBeCalledTimes(1);
    expect(StackdriverLogger.prototype.log).toHaveBeenCalledWith([
      {
        severity: 200,
        labels: {
          deviceKey: "9509915d-e9d3-4d15-bdee-4cdeae24mock"
        },
        jsonPayload: {
          text: testlog1
        }
      },
      {
        severity: 200,
        labels: {
          deviceKey: "9509915d-e9d3-4d15-bdee-4cdeae24mock"
        },
        jsonPayload: {
          text: testlog2
        }
      }
    ]);
  });

  it("Error keeps logs", async (): Promise<void> => {
    // GIVEN
    const INTERVAL = 500;
    StackdriverLogger.prototype.log = jest
      .fn()
      .mockImplementationOnce(() => {
        // eslint-disable-next-line prefer-promise-reject-errors
        return Promise.reject("mocked error");
      })
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      .mockResolvedValue(() => {});

    // WHEN
    const logger = new CloudLogger(INTERVAL, true);
    logger.log("testlog");

    await wait(2.1 * INTERVAL);

    // THEN
    expect(StackdriverLogger.prototype.log).toBeCalledTimes(2);
  });

  it("do not autoflush", async (): Promise<void> => {
    // GIVEN
    const INTERVAL = 500;
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue(() => {});

    // WHEN
    const logger = new CloudLogger(INTERVAL, false);
    logger.log("testlog");

    await wait(1.1 * INTERVAL);

    // THEN
    expect(StackdriverLogger.prototype.log).not.toBeCalled();
  });

  it("do not log device key", async (): Promise<void> => {
    // GIVEN
    const testlog = "foo bar";
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue(() => {});

    // WHEN
    const logger = new CloudLogger(undefined, false, false);
    logger.log(testlog);
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).toHaveBeenCalledWith([
      {
        severity: 200,
        labels: {
          deviceKey: ""
        },
        jsonPayload: {
          text: testlog
        }
      }
    ]);
  });

  it("default log level settings", async (): Promise<void> => {
    // GIVEN
    const testDebuglog = "foo bar";
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue(() => {});

    // WHEN
    const logger = new CloudLogger();
    logger.debug(testDebuglog);
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).not.toBeCalled();
  });

  it("local storage log level settings", async (): Promise<void> => {
    // GIVEN
    localStorage.setItem("app:cloudlogging:level", "DEBUG");
    const testDebuglog = "foo bar";
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue(() => {});

    // WHEN
    const logger = new CloudLogger();
    logger.debug(testDebuglog);
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).toHaveBeenCalledWith([
      {
        severity: 100,
        labels: {
          deviceKey: mockUuid
        },
        jsonPayload: {
          text: testDebuglog
        }
      }
    ]);

    localStorage.removeItem("app:cloudlogging:level");
  });

  it("invalid local storage log level settings", async (): Promise<void> => {
    // GIVEN
    localStorage.setItem("app:cloudlogging:level", "bla"); // <-- should be a valid log level
    const testLog = "foo bar";
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue(() => {});

    // WHEN
    const logger = new CloudLogger();
    logger.log(testLog); // <--- local storage is invaid => falls back to info
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).toHaveBeenCalledWith([
      {
        severity: 200,
        labels: {
          deviceKey: mockUuid
        },
        jsonPayload: {
          text: testLog
        }
      }
    ]);

    localStorage.removeItem("app:cloudlogging:level");
  });

  it("test account logging does not log", async (): Promise<void> => {
    // GIVEN
    localStorage.setItem("app:cloudlogging:testaccount", "whatever");
    const testLog = "foo bar";
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    StackdriverLogger.prototype.log = jest.fn().mockResolvedValue(() => {});

    // WHEN
    const logger = new CloudLogger();
    logger.log(testLog);
    logger.flush();

    // THEN
    expect(StackdriverLogger.prototype.log).toHaveBeenCalledWith([
      {
        severity: 200,
        labels: {
          deviceKey: mockUuid,
          testAccount: "true"
        },
        jsonPayload: {
          text: testLog
        }
      }
    ]);
    localStorage.removeItem("app:cloudlogging:testaccount");
  });
});
