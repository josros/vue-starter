process.env.VUE_APP_VERSION = require("./package.json").version;

module.exports = {
  lintOnSave: false,
  // eslint-disable-next-line prettier/prettier
  publicPath: process.env.NODE_ENV === "production" ? "/vue-starter/" : "/",
  pluginOptions: {
    i18n: {
      locale: "de",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: false
    }
  }
};
