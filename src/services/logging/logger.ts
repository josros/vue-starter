import { CloudLogger } from "./logger-cloud";

interface LoggerConfig {
  cloudLogging: boolean;
}

class Logger {
  public static config: LoggerConfig = {
    cloudLogging: true
  };

  private static cloudLogger: CloudLogger = new CloudLogger(undefined, true);

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public log = (function () {
    console.log(`Initialize log function with cloud logger enabled: ${Logger.config.cloudLogging}`);
    if (Logger.config.cloudLogging) {
      return Function.prototype.bind.call(Logger.cloudLogger.log, Logger.cloudLogger);
    } else {
      return Function.prototype.bind.call(console.log, console);
    }
  })();

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public info = (function () {
    if (Logger.config.cloudLogging) {
      return Function.prototype.bind.call(Logger.cloudLogger.log, Logger.cloudLogger);
    } else {
      return Function.prototype.bind.call(console.info, console);
    }
  })();

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public error = (function () {
    if (Logger.config.cloudLogging) {
      return Function.prototype.bind.call(Logger.cloudLogger.error, Logger.cloudLogger);
    } else {
      return Function.prototype.bind.call(console.error, console);
    }
  })();

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public critical = (function () {
    if (Logger.config.cloudLogging) {
      return Function.prototype.bind.call(Logger.cloudLogger.critical, Logger.cloudLogger);
    } else {
      return Function.prototype.bind.call(console.error, console);
    }
  })();

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public warn = (function () {
    if (Logger.config.cloudLogging) {
      return Function.prototype.bind.call(Logger.cloudLogger.warn, Logger.cloudLogger);
    } else {
      return Function.prototype.bind.call(console.warn, console);
    }
  })();

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public debug = (function () {
    if (Logger.config.cloudLogging) {
      return Function.prototype.bind.call(Logger.cloudLogger.debug, Logger.cloudLogger);
    } else {
      return Function.prototype.bind.call(console.debug, console);
    }
  })();

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public flush = (function () {
    return Function.prototype.bind.call(Logger.cloudLogger.flush, Logger.cloudLogger);
  })();
}

Logger.config = {
  cloudLogging: process.env.VUE_APP_LOGGING_STACKDRIVER_ENABLED
    ? /true/i.test(process.env.VUE_APP_LOGGING_STACKDRIVER_ENABLED)
    : false
};

const appLogger = new Logger();
export default appLogger;
