import { StackdriverLogger } from "@josros/stackdriver-logger";
import uuid from "uuid/v4";

export interface Label {
  key: string;
  value: string;
}

export enum LogLevel {
  CRITICAL = 0,
  ERROR = 1,
  WARNING = 2,
  INFO = 3,
  DEBUG = 4
}

// https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry#LogSeverity
enum GCLogLevel {
  DEFAULT = 0, // (0) The log entry has no assigned severity level.
  DEBUG = 100, // (100) Debug or trace information.
  INFO = 200, // (200) Routine information, such as ongoing status or performance.
  NOTICE = 300, // (300) Normal but significant events, such as start up, shut down, or a configuration change.
  WARNING = 400, // (400) Warning events might cause problems.
  ERROR = 500, // (500) Error events are likely to cause problems.
  CRITICAL = 600, // (600) Critical events cause more severe problems or outages.
  ALERT = 700, // (700) A person must take an action immediately.
  EMERGENCY = 800 // (800) One or more systems are unusable.
}

export class CloudLogger {
  private static readonly STORAGE_DEVICE_KEY = "app:cloudlogging:device";
  private static readonly STORAGE_KEY_TESTACCOUNT = "app:cloudlogging:testaccount";

  private static readonly STORAGE_KEY_LEVEL = "app:cloudlogging:level";

  private static readonly MIN_LOGLEVEL = LogLevel.INFO;
  private readonly stackdriver: StackdriverLogger;
  private logInterval: number;
  private isOperating = false;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private logQueue: any[] = [];
  private deviceKey = "";

  constructor(logInterval = 10000, autoflush = false, rememberDevice = true) {
    this.logInterval = logInterval;
    const issuer = process.env.VUE_APP_LOGGING_STACKDRIVER_CLIENT_EMAIL;
    const pk = process.env.VUE_APP_LOGGING_STACKDRIVER_PK;
    const projectId = process.env.VUE_APP_LOGGING_STACKDRIVER_PROJECT_ID;

    if (!issuer || !pk || !projectId) {
      throw new Error("Required stackdriver config is missing in environment");
    }
    this.stackdriver = new StackdriverLogger(issuer, pk, projectId);
    console.log("Initialized logger");

    this.initDeviceKey(rememberDevice);
    this.initAutoflush(autoflush);
  }

  public log(msg: string, labels?: Label[]): void {
    this.enqueue(msg, LogLevel.INFO, labels);
  }

  public critical(msg: string, labels?: Label[]): void {
    this.enqueue(msg, LogLevel.CRITICAL, labels);
  }

  public error(msg: string, labels?: Label[]): void {
    this.enqueue(msg, LogLevel.ERROR, labels);
  }

  public warn(msg: string, labels?: Label[]): void {
    this.enqueue(msg, LogLevel.WARNING, labels);
  }

  public debug(msg: string, labels?: Label[]): void {
    this.enqueue(msg, LogLevel.DEBUG, labels);
  }

  public flush(): void {
    if (this.logQueue.length > 0 && !this.isOperating) {
      this.isOperating = true;
      this.stackdriver
        .log(this.logQueue)
        .then(() => {
          this.logQueue = [];
        })
        .catch((error) => {
          console.error(error);
        })
        .finally(() => {
          this.isOperating = false;
        });
    }
  }

  private isTestAccount(): boolean {
    const testaccount = localStorage.getItem(CloudLogger.STORAGE_KEY_TESTACCOUNT);
    return !!testaccount;
  }

  private enqueue(logMsg: string, logLevel: LogLevel = LogLevel.INFO, labels?: Label[]): void {
    if (this.shouldEnqueue(logLevel)) {
      const newLog = this.construct(logMsg, logLevel, labels);
      this.logQueue.push(newLog);
    }
  }

  private construct(logMsg: string, logLevel: LogLevel, labels?: Label[]): {} {
    labels = this.defaultLabels(labels);

    return {
      severity: this.convertToGCLevel(logLevel),
      labels: this.constructLabels(labels),
      jsonPayload: {
        text: logMsg
      }
    };
  }

  private defaultLabels(labels?: Label[]): Label[] {
    const isTestAccount = this.isTestAccount();
    if (!labels) {
      labels = [];
    }
    labels.push({ key: "deviceKey", value: this.deviceKey });
    if (isTestAccount) {
      labels.push({ key: "testAccount", value: `${isTestAccount}` });
    }

    return labels;
  }

  private constructLabels(labels?: Label[]): {} {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const labelsObject: any = {};
    if (labels && labels.length > 0) {
      labels.forEach((label) => {
        labelsObject[label.key] = label.value;
      });
    }
    return labelsObject;
  }

  private shouldEnqueue(logLevel: LogLevel): boolean {
    return this.isRelevant(logLevel);
  }

  private isRelevant(logLevel: LogLevel): boolean {
    const storageLevel = localStorage.getItem(CloudLogger.STORAGE_KEY_LEVEL);
    if (storageLevel) {
      const logLevelFromStorage: LogLevel = this.convert(storageLevel);
      return logLevel <= logLevelFromStorage;
    }
    return logLevel <= CloudLogger.MIN_LOGLEVEL;
  }

  private initDeviceKey(enable: boolean): void {
    if (enable) {
      const deviceKey = localStorage.getItem(CloudLogger.STORAGE_DEVICE_KEY);
      if (deviceKey) {
        this.deviceKey = deviceKey;
      } else {
        this.deviceKey = uuid();
        localStorage.setItem(CloudLogger.STORAGE_DEVICE_KEY, this.deviceKey);
      }
    }
  }

  private initAutoflush(enable: boolean): void {
    if (enable) {
      console.log("Autoflush logs is enabled");
      setInterval((): void => {
        this.flush();
      }, this.logInterval);
    }
  }

  private convert(logLevelStr: string): LogLevel {
    switch (logLevelStr) {
      case "ERROR":
      case "error":
        return LogLevel.ERROR;
      case "WARNING":
      case "warning":
        return LogLevel.WARNING;
      case "INFO":
      case "info":
        return LogLevel.INFO;
      case "DEBUG":
      case "debug":
        return LogLevel.DEBUG;
      default:
        return LogLevel.INFO;
    }
  }

  private convertToGCLevel(logLevel: LogLevel): GCLogLevel {
    switch (logLevel) {
      case LogLevel.CRITICAL:
        return GCLogLevel.CRITICAL;
      case LogLevel.ERROR:
        return GCLogLevel.ERROR;
      case LogLevel.WARNING:
        return GCLogLevel.WARNING;
      case LogLevel.INFO:
        return GCLogLevel.INFO;
      case LogLevel.DEBUG:
        return GCLogLevel.DEBUG;
      default:
        return GCLogLevel.DEFAULT;
    }
  }
}
