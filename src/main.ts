import Vue, { VNode } from "vue";
import App from "./App.vue";
import router from "./router/router";
import i18n from "./i18n";
import { LoggingPlugin } from "./plugins/logging";

Vue.use(LoggingPlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  i18n,
  render: (h): VNode => h(App)
}).$mount("#app");
