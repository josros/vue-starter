import _Vue from "vue"; // <-- notice the changed import
import appLogger from "@/services/logging/logger";

// export type PluginFunction<T> = (Vue: typeof _Vue, options?: T) => void;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function LoggingPlugin(Vue: typeof _Vue): void {
  Vue.prototype.$log = appLogger.log;
  Vue.prototype.$info = appLogger.info;
  Vue.prototype.$critical = appLogger.critical;
  Vue.prototype.$error = appLogger.error;
  Vue.prototype.$warn = appLogger.warn;
  Vue.prototype.$debug = appLogger.debug;
  Vue.prototype.$flush = appLogger.flush;
}
