import Vue from "vue";

declare module "vue/types/vue" {
  interface Vue {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $log: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $info: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $critical: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $error: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $warn: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $debug: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    $flush: any;
  }
}
