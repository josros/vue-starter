export class GlobalEvent {
  public static readonly SUCCESS_EVENT: string = "successEvent";
  public static readonly WARNING_EVENT: string = "warningEvent";
  public static readonly ERROR_EVENT: string = "errorEvent";
}
