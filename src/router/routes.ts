import { RouteConfig } from "vue-router";
import Welcome from "../views/Welcome.vue";
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const Imprint = () => import("../views/Imprint.vue");

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "welcome",
    component: Welcome
  },
  {
    path: "/imprint",
    name: "imprint",
    component: Imprint
  }
];

export default routes;
