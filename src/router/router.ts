import Vue from "vue";
import Router from "vue-router";
import routes from "./routes";
import appLogger from "@/services/logging/logger";

Vue.use(Router);

const router = new Router({
  routes,
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  scrollBehavior: (to) => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

router.beforeEach(
  async (to, from, next): Promise<void> => {
    appLogger.info(`Go to route: ${to.name}`, [{ key: "category", value: "ROUTING" }]);
    next();
  }
);

export default router;
